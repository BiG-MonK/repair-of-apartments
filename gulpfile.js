const gulp          = require('gulp'),
      cleanCSS      = require('gulp-clean-css'),
      htmlmin       = require('gulp-htmlmin'),
      del           = require('del'),
      imagemin      = require('gulp-imagemin'),
      imgCompress   = require('imagemin-jpeg-recompress');

gulp.task('del', () => 
  del(['dist/**', '!dist'])
);

gulp.task('minify-css', () => 
  gulp.src('./src/css/*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist/css'))
)

gulp.task('move-js', () => 
  gulp.src('./src/js/*.js')
    .pipe(gulp.dest('dist/js'))
)

gulp.task('htmlmin', () => 
  gulp.src('./src/*.html')
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest('dist'))
)

gulp.task('move-fonts', () => 
  gulp.src('./src/fonts/**/*')
    .pipe(gulp.dest('dist/fonts'))
)

// gulp.task('move-img', () => 
//   gulp.src('./src/img/**/*.png')
//     .pipe(gulp.dest('dist/img'))
// )

gulp.task('imgmin', () => {
  return gulp.src('src/img/**/*')
  .pipe(imagemin([
    imgCompress({
      loops: 4,
      min: 70,
      max: 80,
      quality: 'high'
    }),
    imagemin.gifsicle(),
    imagemin.optipng(),
    imagemin.svgo()
  ]))
  .pipe(gulp.dest('dist/img'));
});

gulp.task('default', gulp.series('del', 'minify-css', 'move-js', 'htmlmin', 'move-fonts', 'imgmin', function(done){
  done();
}));